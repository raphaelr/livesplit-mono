#!/bin/sh
set -e
p=LiveSplit/.nuget/nuget.exe
if ! [ -f "$p" ]; then
    echo "Downloading nuget.exe..."
    curl 'https://dist.nuget.org/win-x86-commandline/latest/nuget.exe' -o "$p"
    chmod u+x "$p"
fi
