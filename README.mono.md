# LiveSplit for Mono on Linux

The patches in this repository port LiveSplit to Mono instead of the .NET
Framework. Wine is still required (for now).

## Dependencies

* quilt - For applying the patches in this repository
* mono-devel - For building LiveSplit
* [Wine](https://www.winehq.org/)
* [wine-mono](https://github.com/madewokherd/wine-mono/releases) - Install
  using `wine msiexec /i wine-mono-WHATEVER.msi`

Quilt, mono-devel, and wine should be in your distribution's repository.

## Usage

Apply the patches:

    quilt push -a

Download nuget.exe (only needs to be done once):

   ./contrib/prepare.sh

Build LiveSplit:

    cd LiveSplit
    xbuild LiveSplit.sln

And run it:

    wine ./bin/Debug/LiveSplit.exe

## License

These patches are licensed under the MIT license, just like LiveSplit itself.
See the LICENSE file.
